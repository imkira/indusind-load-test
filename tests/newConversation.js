import http from "k6/http";
import { check, sleep, fail } from "k6";
import { Rate } from "k6/metrics";
import { group } from "k6";
var config = require("../config.js");
var dataProvider = require("../data/dataProvider.js");

export let errorRate = new Rate("errors");

export let options = {
    vus: 1,
    iterations: 1,
    batch: 10
};
var moduleCounter = 0;
export default function() {
    group("Conversation flow", function() {
        group("Create New Conversation", function() {                  
            sendRequest(config.BaseUrl+"conversations",dataProvider.conversation.newConversation);
        });
        startNewModule(dataProvider.conversation.newModule[moduleCounter]);    
      });
}

var currentModule;
var messageCounter = 0;
function startNewModule(newModule) {
    if(!newModule) {
        console.log("Module End");        
        return;
    }
    currentModule = newModule;
    messageCounter = 0;
    group("Start New Module "+newModule.module, function() {
        sendRequest(config.BaseUrl+"conversations/"+dataProvider.conversationId+"/activities",newModule.module,"next");
    });
}

function sendNextMessage(message) {
    var type = "";
    if(message) {
        type = "next";        
        group("Push Message"+message, function() {
            sendRequest(config.BaseUrl+"conversations/"+dataProvider.conversationId+"/activities",message,type);
        });
    } else {
        //message update done
        messageCounter = 0;
        moduleCounter++;
        startNewModule(dataProvider.conversation.newModule[moduleCounter])
    }
}

function sendRequest(url, payload, type) {    
    var params =  { headers: { "Authorization": "Basic "+config.accessToken,
                    "Accept": "application/json", "Content-Type": "application/json"} };    
    var res = http.post(url, JSON.stringify(payload), params);    
    reqCheck(res,type);    
}

function reqCheck(response,type) {
    check(response, {
        "status is 200": (r) => {
            console.log("status:"+r.status);
            printResponseLogs(r);
            if(r.status == 200) {
                if(type == "next") {                    
                    sendNextMessage(currentModule.message[messageCounter]);
                    messageCounter++;
                }
            }
            return r.status == 200;
        }
    })  || errorRate.add(1);
}


function printResponseLogs(res) {
    console.log("Request Url - "+res.url);
    console.log("Response time was " + String(res.timings.duration) + " ms");
    if(res.status !=200) {
        console.log("Error:"+res.body);
    }
}