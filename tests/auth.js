import http from "k6/http";
import { check, sleep, fail } from "k6";
import { Rate } from "k6/metrics";
import { group } from "k6";
var config = require("../config.js");

var accessToken = "";

export let errorRate = new Rate("errors");

export let options = {
    vus: 1,
    iterations: 1
};

export default function() {
    checkAuth();     
};

function checkAuth() {
    var url = config.BaseUrl+"auth";
    var payload = JSON.stringify({
        "email":"Iblfm5102@indusind.com",
        "password":"sai#1206",
        "logoutOtherDevices": true
        });        
    var params =  { headers: { "Content-Type": "application/json" } };    
    var res = http.post(url, payload, params);    
    reqCheck(res);
    printResponseLogs(res);             
}

function reqCheck(response) {
    check(response, {
        "status is 200": (r) => {
            console.log("status:"+r.status);            
            return r.status == 200;
        }
    }) || fail("status code was not 200") || errorRate.add(1);
}

function printResponseLogs(res) {
    console.log("Request Url - "+res.url);
    console.log("Response time was " + String(res.timings.duration) + " ms");
    console.log("accessToken - "+JSON.parse(res.body).accessToken);
    accessToken = JSON.parse(res.body).accessToken;
    console.log("config");
}