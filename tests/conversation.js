import http from "k6/http";
import { check, sleep, fail} from "k6";
import { Rate} from "k6/metrics";
import { group} from "k6";

var config = require("../config.js");
var dataProvider = require("../data/dataProvider.js");

export let errorRate = new Rate("errors");

export let options = {
    vus: 1,
    iterations: 1
};

var moduleCounter = 0;
var applicationStack = dataProvider.conversation.session.applicationStack;
var modules = dataProvider.conversation.modules;
var conversationId;
var conversationList = [];

export default function () {
    console.log(JSON.stringify(dataProvider.conversation.from.id));
    if (applicationStack[0] == "module") {
        applicationStack = applicationStack.slice(1);
        group("Create New Conversation", function () {
            conversationId = generateConversationId();
            conversationList.push({conversationId:conversationId});
            sendRequest(config.BaseUrl + "conversations", generateConversation(conversationId));
        });
    } else {
        group("Starting with previous Conversation", function () {
            conversationId = "";
            applicationNumber = "";
            sendNextState(moduleCounter);
        });
    }
}
var messageCounter = 0;

function sendNextState(position) {
    if (applicationStack.length - 1 < position) {
        console.log("Conversation Send Successfully");
        console.log("ConversationList:"+JSON.stringify(conversationList));
        return;
    }
    var state = applicationStack[position];
    console.log("position-" + position + ": state-" + state);
    var messageQueue = state.split(".");
    if (messageQueue[2] == "start") {
        messageCounter = 0;
        startNewModule(applicationStack[position]);
    } else {
        sendNewMessage(applicationStack[position]);
    }
}

function startNewModule(state) {
    var messageQueue = state.split(".");
    group("Start New Module " + messageQueue[1], function () {
        var start = {
            "type": "message",
            "message": {
                type: "text",
                subModuleID: messageQueue[1],
                moduleID: messageQueue[0],
                payload: {}
            }
        };
        sendRequest(config.BaseUrl + "conversations/" + conversationId + "/activities", start);
    });
}

function sendNewMessage(state) {
    var messageQueue = state.split(".");
    var message;
    modules[messageQueue[0]][messageQueue[1]].messages.forEach(function (m) {        
        if (m.state == messageQueue[2]) {
            message = m.response;
        }
    });
    if (!message) {
        console.log("Payload not availiable for " + state);
        sendNextState(moduleCounter++);
        return;
    }
    formatData(message);
    messageCounter++;
    var payload = {
        message: message,
        moduleData: {},
        type: "message"
    };    
    console.log("messageQueue state:" + messageQueue[2] + " payload state:" + message.state);
    sendRequest(config.BaseUrl + "conversations/" + conversationId + "/activities", payload);
}

function formatData(message) {
    // format message with dynamic data
    if(message.payload.ApplicationNumber) {
        message.payload.ApplicationNumber = generateApplicationNumber();
    }
}

function sendRequest(url, payload) {
    var params = {
        headers: {
            "Authorization": "Basic " + config.accessToken,
            "Accept": "application/json",
            "Content-Type": "application/json"
        }
    };
    var res = http.post(url, JSON.stringify(payload), params);
    reqCheck(res);
}

function reqCheck(response) {
    check(response, {
        "status is 200": (r) => {            
            printResponseLogs(r);
            if (r.status == 200) {
                if (r.url == config.BaseUrl + "conversations") {
                    var start = {
                        "type": "message",
                        "message": {
                            "type": "text",
                            "payload": {}
                        }
                    };
                    sendRequest(config.BaseUrl + "conversations/" + conversationId + "/activities", start);
                } else {
                    console.log("Starting new Message....");                    
                    sendNextState(moduleCounter++);
                }
            }
            return r.status == 200;
        },
        "Message Sent faild": (r) => {
            if(r.status!=200){
                printResponseLogs(r);
                return true;
            }
        }
    }) || errorRate.add(1);
}

function printResponseLogs(res) {    
    console.log("Status:" + res.status);
    console.log("Response time was " + String(res.timings.duration) + " ms");
    if(res.status != 200) {
       console.log("Response with Error"); 
    }
}

function generateConversation(conversationId) {
    return {
        conversationId: conversationId,
        id: conversationId,
        "orgId": dataProvider.conversation.orgId,
        "dialogId": dataProvider.conversation.dialogId,
        "dialogVersion": dataProvider.conversation.dialogVersion,
        "dialogProps": {
            "title": "applicantName",
            "subtitle": "mobileNo"
        },
        "dialogName": "indusind",
        "from": {
            "id": dataProvider.conversation.from.id,
            "environmentInfo": {
                "macAddress": "10:07:B6:B3:80:86",
                "time": "2018-12-06 11:07:45.576",
                "startLocation": {
                    "longitude": 80.2420991,
                    "latitude": 12.9909489,
                    "time": "2018-12-06 11:07:45.581"
                }
            }
        },
        "modules": {},
        "messages": [],
        "createdAt": new Date().toISOString(),
        "manualReviewRequired": false,
        "applicationId": "TMP6977"
    };
}

function generateConversationId() {
    var timestamp = (new Date().getTime() / 1000 | 0).toString(16);
    return timestamp + 'xxxxxxxxxxxxxxxx'.replace(/[x]/g, function () {
        return (Math.random() * 16 | 0).toString(16);
    }).toLowerCase();
}

function generateApplicationNumber() {
    return "AC" + (Math.random() * 9999999).toFixed();
}